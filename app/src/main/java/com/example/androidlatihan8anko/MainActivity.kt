package com.example.androidlatihan8anko

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*

class MainActivity : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            val username = etUsername.text.toString().trim()
            val password = etPassword.text.toString().trim()
            handlingForLogin(username, password)
        }
        tvRegister.setOnClickListener {
            toast("Pindah Halaman Register")
            startActivity(intentFor<Register>())
        }
    }

    private fun handlingForLogin(username: String, password: String) {
        info { "username = $username , password = $password" }
        if (username.equals("master") && password.equals("master123")) {
            toast("Welcome $username")
        } else {
            alert(title = "Warning", message = "Username atau Password Salah") {
                positiveButton(buttonText = "OK") {
                    //Nothing
                }
                negativeButton(buttonText = "CANCEL") {
                    //Kepo
                }
                isCancelable = false
            }.show()
        }
    }
}
