package com.example.androidlatihan8anko.AnkoLayout

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.androidlatihan8anko.AfterClass
import com.example.androidlatihan8anko.R
import com.example.androidlatihan8anko.Register
import org.jetbrains.anko.*

class Main2 : AppCompatActivity(), AnkoLogger {
    override fun onCreate(savedInstanceState: Bundle?) {
        ViewLogin().setContentView(this)
        super.onCreate(savedInstanceState)
        val btn: Button = findViewById(R.id.btnLogin)
        val reg: TextView = findViewById(R.id.tvRegister)
        val un: EditText = findViewById(R.id.etUsername)
        val pw: EditText = findViewById(R.id.etPassword)
        btn.setOnClickListener {
            val un = un.text.toString().trim()
            val pw = pw.text.toString().trim()
            handlingForLogin(un, pw)
        }
        reg.setOnClickListener {
            startActivity(intentFor<Register>())
        }
    }
    class ViewLogin : AnkoComponent<Main2> {
        override fun createView(ui: AnkoContext<Main2>) = with(ui) {
            verticalLayout {
                lparams {
                    width = matchParent
                    height = matchParent
                    padding = 30
                    verticalGravity = Gravity.CENTER
                    backgroundColor = Color.DKGRAY
                }
                verticalLayout {
                    lparams(width = matchParent, height = wrapContent) {
                        backgroundColor = Color.WHITE
                        margin = 15
                        padding = 20
                    }
                    textView {
                        text = "LOGIN"
                        textSize = 25.0f
                        textColor = Color.CYAN
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                    editText {
                        hint = "Input Your Name"
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                        setHintTextColor(Color.GRAY)
                        textColor = Color.BLACK
                        inputType = InputType.TYPE_CLASS_TEXT
                        id = R.id.etUsername
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                    editText {
                        setHintTextColor(Color.GRAY)
                        textColor = Color.BLACK
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                        hint = "Input Your Password"
                        inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                        id = R.id.etPassword
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                    button("LOGIN") {
                        id = R.id.btnLogin
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                    textView {
                        text = "Register!!"
                        id = R.id.tvRegister
                        textSize = 15.0f
                        textColor = Color.BLUE
                        textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                    }.lparams(width = matchParent, height = wrapContent) {
                        margin = 15
                    }
                }
            }
        }
    }

    private fun handlingForLogin(un: String, pw: String) {
        info { "username = $un , password = $pw" }
        if (un.equals("master") && pw.equals("master123")) {
            toast("Welcome $un")
            startActivity(intentFor<AfterClass>("nama" to un))
            finish()
        } else {
            alert(title = "Warning", message = "Username atau Password Salah") {
                positiveButton(buttonText = "OK") {
                    //Nothing
                }
                negativeButton(buttonText = "CANCEL") {
                    //Kepo
                }
                isCancelable = false
            }.show()
        }
    }
}