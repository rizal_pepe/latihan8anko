package com.example.androidlatihan8anko

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.loginafter.*

class AfterClass : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.loginafter)
        super.onCreate(savedInstanceState)
        nama.text = intent.getStringExtra("nama")
    }
}