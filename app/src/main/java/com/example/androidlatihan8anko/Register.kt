package com.example.androidlatihan8anko

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.alert
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

class Register : AppCompatActivity(), AnkoLogger {
    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_register)
        super.onCreate(savedInstanceState)
        btnRegistrasi.setOnClickListener {
            val usernameRegister = etUsernameRegister.text.toString().trim()
            val passwordRegister = etPasswordRegister.text.toString().trim()
            handlingForRegister(usernameRegister, passwordRegister)
        }
        tvLogin.setOnClickListener {
            toast("Pindah Halaman Login")
            startActivity(intentFor<MainActivity>())
        }
    }

    private fun handlingForRegister(usernameRegister: String, passwordRegister: String) {
        when {
            usernameRegister.isEmpty() -> toast("Username Kosong")
            passwordRegister.isEmpty() -> toast("Password Kosong")
            else -> alert(title = "Register Sukses", message = "User Berhasil Ditambahkan") {
                positiveButton(buttonText = "OK") {
                    onBackPressed()
                    finish()
                }
                isCancelable = false
            }.show()
        }
    }
}